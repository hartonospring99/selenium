package twse.brs;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.bytedeco.javacpp.opencv_core;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.io.FileHandler;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

public class TestOJK {
    public static void main(String[] args) throws InterruptedException, IOException, TesseractException {

        ChromeOptions options = new ChromeOptions();
        //options.addArguments("--start-maximized");
        //System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        //options.addArguments("force-device-scale-factor=0.95");
        options.addArguments("high-dpi-support=1.2");

        WebDriver driver;
        System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
        driver=new ChromeDriver(options);

        driver.get("https://slik.ojk.go.id/slik/captcha/");
        WebElement imageElement = driver.findElement(By.xpath("//body//img"));


        Thread.sleep(1000);
        File src = imageElement.getScreenshotAs(OutputType.FILE);
        Thread.sleep(1000);
        String path = "/home/tonk/Documents/PROJECT/TEST/twse-captcha-solver-java-master/src/main/resources" +
                "/captchaImage/captchaOJK.png";

        FileHandler.copy(src, new File(path));
        Thread.sleep(2000);

//        File input = new File(path);
//        BufferedImage imageOpencv = ImageIO.read(input);
//        System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
//
//        byte[] data = ((DataBufferByte) imageOpencv.getRaster().getDataBuffer()).getData();
//        Mat mat = new Mat(imageOpencv.getHeight(), imageOpencv.getWidth(), CvType.CV_8UC3);
//        mat.put(0, 0, data);
//
//        Mat mat1 = new Mat(imageOpencv.getHeight(),imageOpencv.getWidth(),CvType.CV_8UC1);
//        Imgproc.cvtColor(mat, mat1, Imgproc.COLOR_RGB2GRAY);
//
//        byte[] data1 = new byte[mat1.rows() * mat1.cols() * (int)(mat1.elemSize())];
//        mat1.get(0, 0, data1);
//        BufferedImage image1 = new BufferedImage(mat1.cols(),mat1.rows(), BufferedImage.TYPE_BYTE_GRAY);
//        image1.getRaster().setDataElements(0, 0, mat1.cols(), mat1.rows(), data1);
//
////        File ouptut = new File("grayscale.png");
////        ImageIO.write(image1, "png", ouptut);



        ITesseract image = new Tesseract();
        //image.setDatapath("/usr/share/tesseract-ocr/4.00/tessdata");
        image.setDatapath("/home/tonk/Documents/PROJECT/TEST/twse-captcha-solver-java-master/tessdata");
        Thread.sleep(1000);
        String str = image.doOCR(new File(path));
        //.replaceAll("[^0-9-A-Z]","");
        System.out.println("image OCR DONE!!!");
        System.out.println("KODE CAPTCHA = "+str);
    }
}

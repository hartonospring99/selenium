package twse.brs;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.concurrent.TimeUnit;

public class GetCookies {
    public static void main(String[] args) {
        System.out.println(" Get COOKIES -Load-");

        WebDriver driver;
        System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
        driver=new ChromeDriver();

//        WebDriverManager.chromedriver().setup();
//        ChromeDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("http://pmb.unpam.ac.id/");
        driver.findElement(By.xpath("//button[normalize-space()='LOGIN PMB']")).click();
        driver.findElement(By.xpath("//div[@id='myModal2']//iframe[@title='reCAPTCHA']")).click();


        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(2500));
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(
                By.xpath("//iframe[starts-with([@name](http://twitter.com/name), 'a-') and starts-with([@src](http://twitter.com/src), '[https://www.google.com/recaptcha'](https://www.google.com/recaptcha'")));

        File file = new File("Cookies.data");

        try {
            // Delete old file if exists
            file.delete();
            file.createNewFile();
            FileWriter fileWrite = new FileWriter(file);
            BufferedWriter Bwrite = new BufferedWriter(fileWrite);
            // loop for getting the cookie information

            // loop for getting the cookie information
            for (Cookie ck : driver.manage().getCookies()) {
                Bwrite.write((ck.getName() + ";" + ck.getValue() + ";" + ck.getDomain() + ";" + ck.getPath() + ";" + ck.getExpiry() + ";" + ck.isSecure()));
                System.out.println(ck);
                Bwrite.newLine();
            }
            Bwrite.close();
            fileWrite.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //driver.close();
    }

}

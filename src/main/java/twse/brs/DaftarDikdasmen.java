package twse.brs;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.apache.commons.io.input.TaggedReader;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.io.FileHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class DaftarDikdasmen {
    public static void main(String[] args) throws InterruptedException, IOException, TesseractException {

        System.out.println("test");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("high-dpi-support=1.2");

        String mail = "robot@gmail.com";
        String nama = "Robot 2022";
        String tgl = "01/01/1990";
        String neg = "Indonesia";
        String ktp = "123.123.123.123";
        String nop = "01.123.456.7-521.000";
        String kerja = "Pegawai Swasta";
        String aI = "Jl. Bersama Biar Gak Sepi";
        String aL = "Street Street Street No. 21";
        String telp = "021-9876543";
        String pass = "RahasiaDonk";

        WebDriver driver;
        System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
        driver=new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.get("http://e-layanan.dikdasmen.kemdikbud.go.id/elayanan2019/daftar");
        WebElement email = driver.findElement(new By.ByXPath("//input[@name='email']"));
        WebElement ulangEmail = driver.findElement(new By.ByXPath("//input[@name='ulang_email']"));
        WebElement namaLengkap = driver.findElement(new By.ByXPath("//input[@placeholder='Ahmad Rahmadi Maulana']"));
        WebElement tglLahir = driver.findElement(new By.ByXPath("//input[@name='tanggal_lahir']"));
        driver.findElement(new By.ByXPath("//div[@class='filter-option-inner-inner']")).click();
        driver.findElement(By.xpath("//li[104]//a[1]")).click();
        WebElement negara = driver.findElement(new By.ByXPath("//input[@autocomplete='off']"));
        WebElement noKtp = driver.findElement(new By.ByXPath("//input[@placeholder='123.123.123.123']"));
        WebElement npwp = driver.findElement(new By.ByXPath("//input[@placeholder='01.123.456.7-521.000']"));
        WebElement pekerjaan = driver.findElement(new By.ByXPath("//input[@placeholder='Pegawai Swasta']"));
        WebElement alamatIndo = driver.findElement(new By.ByXPath("//textarea[@name='alamat_indo']"));
        WebElement alamatLuar = driver.findElement(new By.ByXPath("//textarea[@name='alamat_luar']"));
        WebElement noTelp = driver.findElement(new By.ByXPath("//input[@placeholder='021-9876543']"));
        WebElement password = driver.findElement(new By.ByXPath("//input[@placeholder='Password']"));
        WebElement ulangPassword = driver.findElement(new By.ByXPath("//input[@placeholder='Ulang Password']"));

        email.sendKeys(mail);
        ulangEmail.sendKeys(mail);
        namaLengkap.sendKeys(nama);
        tglLahir.sendKeys(tgl);
        negara.click();
        noKtp.sendKeys(ktp);
        npwp.sendKeys(nop);
        pekerjaan.sendKeys(kerja);
        alamatIndo.sendKeys(aI);
        alamatLuar.sendKeys(aL);
        noTelp.sendKeys(telp);
        password.sendKeys(pass);
        ulangPassword.sendKeys(pass);
        Thread.sleep(2000);

        driver.findElement(By.xpath("//a[@class='btn btn-success pull-left']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[@class='btn btn-block btn-info']")).click();
        Thread.sleep(2000);
        WebElement userIn = driver.findElement(By.xpath("//input[@id='identity']"));
        WebElement passw = driver.findElement(By.xpath("//input[@id='password-field']"));
        userIn.sendKeys(mail);
        passw.sendKeys(pass);
        Thread.sleep(2000);
        WebElement imageElement = driver.findElement(By.xpath("//img[@alt=' ']"));

        Thread.sleep(5000);
        File src = imageElement.getScreenshotAs(OutputType.FILE);
        Thread.sleep(2000);
        String path = "/home/tonk/Documents/PROJECT/TEST/twse-captcha-solver-java-master/src/main/resources" +
                "/captchaImage/captchaDikdasmen.png";



        FileHandler.copy(src, new File(path));
        Thread.sleep(2000);
//        BufferedImage img = null;
//        try{
//            img = ImageIO.read(path);
//        }catch(IOException e){
//            System.out.println(e);
//        }
//        int width = img.getWidth();
//        int height = img.getHeight();

        ITesseract image = new Tesseract();
        image.setDatapath("/usr/share/tesseract-ocr/4.00/tessdata");
        //image.setDatapath("/home/tonk/Documents/PROJECT/TEST/twse-captcha-solver-java-master/tessdata");
        Thread.sleep(1000);

        String str = image.doOCR(new File(path)).replaceAll("[^0-9-A-Z]", "");
        System.out.println("image OCR DONE!!!");
        System.out.println("KODE CAPTCHA = "+str);

        WebElement captchaImage = driver.findElement(By.xpath("//input[@id='capcha']"));
        captchaImage.sendKeys(str);
        Thread.sleep(15000);

    }
}

package twse.brs;


import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.io.FileHandler;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.*;

public class UnpamTest {
    public static void main(String[] args) {
        System.out.println("test");


        try {
            ChromeOptions options = new ChromeOptions();
            //options.addArguments("--start-maximized");
            //System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
            //options.addArguments("force-device-scale-factor=0.95");
            options.addArguments("high-dpi-support=1.2");

            WebDriver driver;
            System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
            driver=new ChromeDriver(options);

            //driver.manage().window().maximize();
            //driver.manage().window().setSize(new Dimension(400, 700));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


            driver.get("http://pmb.unpam.ac.id/");
            driver.findElement(new ByXPath("//button[normalize-space()='LOGIN PMB']")).click();
            Thread.sleep(1000);
            WebElement username = driver.findElement(new ByXPath("//input[@name='username']"));
            WebElement password = driver.findElement(new ByXPath("//input[@type='password']"));
            username.sendKeys("202219003021");
            password.sendKeys("123456");

            Thread.sleep(1000);
            WebElement imageElement = driver.findElement(By.xpath("//div[@id='container1']//img[@alt=' ']"));

            //WebElement imageElement = driver.findElement(By.xpath("//h4[normalize-space()='FORM LOGIN']"));
            //File src = imageElement.getScreenshotAs(OutputType.FILE);
            Thread.sleep(5000);
            File src = imageElement.getScreenshotAs(OutputType.FILE);
            Thread.sleep(2000);
            String path = "/home/tonk/Documents/PROJECT/TEST/twse-captcha-solver-java-master/src/main/resources" +
                    "/captchaImage/captcha.png";

            FileHandler.copy(src, new File(path));
            Thread.sleep(2000);

            ITesseract image = new Tesseract();
            //image.setDatapath("/usr/share/tesseract-ocr/4.00/tessdata");
            image.setDatapath("/home/tonk/Documents/PROJECT/TEST/twse-captcha-solver-java-master/tessdata");
            Thread.sleep(1000);
            String str = image.doOCR(new File(path)).replaceAll("[^0-9]","");
            System.out.println("image OCR DONE!!!");
            System.out.println("KODE CAPTCHA = "+str);

            WebElement captcha = driver.findElement(By.xpath("//form[@action='http://pmb.unpam.ac.id/c_login/cek_login']//input[@placeholder='Tuliskan seluruh angka yang ada di gambar']"));
            captcha.sendKeys(str);
            Thread.sleep(15000);
//            driver.findElement(By.xpath("//button[normalize-space()='Login']")).click();
//            Thread.sleep(1000);
//            driver.switchTo().alert().accept();

            System.out.println("Proses Selesai!!!!");

        } catch (Exception e){
            System.out.println("Exception : " +e.getMessage());
        }

    }
}

